CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module installs a Fullcalendar View preconfigured to display Event products,
of a product type also defined by this module. Although Smart Date supports
recurring events, this module does not, since the date and time are specified as
single value field on a product variation, which allows for multiple variations
to be defined, for separate registrattion to each date/time.

This can also be used to create registration for events that do not require
payment. For this use case, define a simple payment gateway using the "Manual"
plugin, setting it to only be available for products where the price is zero.
It may also be optimal to create a simplified checkout flow for this scenario.


INSTALLATION
------------

 * Install the Smart Date, Fullcalendar Views, and Cmmerce modules as you would
   normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.
 * Then, install the Commerce module and the Product submodule. It will likely
   be necessary to enable additional submodules (e.g. Cart, Checkout, etc) to
   have a fully functional checkout system.
 * Finally, install Smart Date Registration Kit. This will import a view
   configured to show Smart Date product events in a Fullcalendar presentation.

- OR -

If using composer and drush, run
`composer require drupal/smart_date_registration_kit`
and then `drush en smart_date_registration_kit`. This will download and install all
the necessary modules.

Post installation (by either method) you can safely uninstall both Smart Date
Starter Kit and Smart Date Calendar Kit without losing the configuration
imported.


REQUIREMENTS
------------

This module requires the Smart Date, Fullcalendar View, and Commerce modules.


CONFIGURATION
-------------

 * This module doesn't currently create a store, payment, or any other standard
   configuration normally needed for an online shopping experience. See the
   Commerce documentation at https://docs.drupalcommerce.org/commerce2/user-guide/
   for additional details on the required setup.


MAINTAINERS
-----------

 * Current Maintainer: Martin Anderson-Clutz (mandclu) -
   https://www.drupal.org/u/mandclu
